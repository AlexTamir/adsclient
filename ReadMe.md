<!-- Improved compatibility of back to top link: See: https://github.com/othneildrew/Best-README-Template/pull/73 -->
<a name="readme-top"></a>
<!--
*** Thanks for checking out the Best-README-Template. If you have a suggestion
*** that would make this better, please fork the repo and create a pull request
*** or simply open an issue with the tag "enhancement".
*** Don't forget to give the project a star!
*** Thanks again! Now go create something AMAZING! :D
-->

<!-- PROJECT LOGO -->
<br />
<div align="center">
  <a href="https://git.alexander-karantzidis.de/all/adsclient">
    <img src="images/ads_logo.png" alt="Logo" width="80" height="80">
  </a>

<h3 align="center">Super Cool ADS Client</h3>

  <p align="center">
    THis project is meant to create a simple to use ADS client to handle different aspects of the daily work with Beckhoff PLCs. Including File Acces, but also some diagnostic features.
    <br />
    <a href="https://git.alexander-karantzidis.de/all/adsclient"><strong>Explore the docs »</strong></a>
    <br />
    <br />
    <a href="https://git.alexander-karantzidis.de/all/adsclient">View Demo</a>
    ·
    <a href="https://git.alexander-karantzidis.de/all/adsclient/issues/new?labels=bug&template=bug-report---.md">Report Bug</a>
    ·
    <a href="https://git.alexander-karantzidis.de/all/adsclient/issues/new?labels=enhancement&template=feature-request---.md">Request Feature</a>
  </p>
</div>



<!-- TABLE OF CONTENTS -->
<details>
  <summary>Table of Contents</summary>
  <ol>
    <li>
      <a href="#about-the-project">About The Project</a>
    </li>
    <li>
      <a href="#getting-started">Getting Started</a>
      <ul>
        <li><a href="#prerequisites">Prerequisites</a></li>
        <li><a href="#installation">Installation</a></li>
      </ul>
    </li>
    <li><a href="#usage">Usage</a></li>
    <li><a href="#roadmap">Roadmap</a></li>
    <li><a href="#contributing">Contributing</a></li>
    <li><a href="#license">License</a></li>
    <li><a href="#contact">Contact</a></li>
    <li><a href="#acknowledgments">Acknowledgments</a></li>
  </ol>
</details>



<!-- ABOUT THE PROJECT -->
## About The Project

![Product Name Screen Shot](images/sample_screenshot.png)

This tool is meant to assist in the daily challenges using Beckhoff PLCs. Included are simple file handling functions to copy files to and from the PLC, delete and rename them, etc. In addition some diagnosis functions are build in to gather system information, information about installed software and many more.

<p align="right">(<a href="#readme-top">back to top</a>)</p>


<!-- GETTING STARTED -->
## Getting Started

Just download the binary and use it!

### Prerequisites

DotNet installed

### Installation

1. Have fun

<p align="right">(<a href="#readme-top">back to top</a>)</p>



<!-- USAGE EXAMPLES -->
## Usage

TBD

<p align="right">(<a href="#readme-top">back to top</a>)</p>



<!-- ROADMAP -->
## Roadmap

- [ ] Functionality
- [ ] The Rest

See the [open issues](https://git.alexander-karantzidis.de/all/adsclient/issues) for a full list of proposed features (and known issues).

<p align="right">(<a href="#readme-top">back to top</a>)</p>



<!-- CONTRIBUTING -->
## Contributing

Contributions are what make the open source community such an amazing place to learn, inspire, and create. Any contributions you make are **greatly appreciated**.

If you have a suggestion that would make this better, please fork the repo and create a pull request. You can also simply open an issue with the tag "enhancement".
Don't forget to give the project a star! Thanks again!

1. Fork the Project
2. Create your Feature Branch (`git checkout -b feature/AmazingFeature`)
3. Commit your Changes (`git commit -m 'Add some AmazingFeature'`)
4. Push to the Branch (`git push origin feature/AmazingFeature`)
5. Open a Pull Request

<p align="right">(<a href="#readme-top">back to top</a>)</p>



<!-- LICENSE -->
## License

Distributed under the MIT License. See `LICENSE.txt` for more information.

<p align="right">(<a href="#readme-top">back to top</a>)</p>



<!-- CONTACT -->
## Contact

Please donÄt contact us!

Project Link: [https://git.alexander-karantzidis.de/all/adsclient](https://git.alexander-karantzidis.de/all/adsclient)

<p align="right">(<a href="#readme-top">back to top</a>)</p>



<!-- ACKNOWLEDGMENTS -->
## Acknowledgments

* []()
* []()
* []()

<p align="right">(<a href="#readme-top">back to top</a>)</p>