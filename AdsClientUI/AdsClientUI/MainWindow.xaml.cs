﻿using System.Windows;
using System.Windows.Controls;
using Microsoft.Extensions.Logging;

namespace AdsClientUI
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();

            InitLogger();

            AddExisitingRoutesToCmbBx();
        }

        private void InitLogger()
        {
            
        }

        private void AddExisitingRoutesToCmbBx()
        {
            //To-Do:
            //Get routes with Mathis function
            List<string> routes = new List<string>();
            routes.Add("CX-12345");
            routes.Add("CX-23456");
            routes.Add("CX-34567");
            routes.Add("CX-12345");

            foreach (string route in routes)
                CmbBx_SelectRoute.Items.Add(route);
        }

        private void CmbBx_SelectRoute_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            //To-Do:
            //Update Textbox of selected Tab


        }
    }
}